Working with Rancher Desktop

## Installation
https://docs.rancherdesktop.io/getting-started/installation

Nice, slick UI

The default container runtime is containerd. Since we use docker-compose for local development, we
must switch to dockerd for use with docker + docker-compose.

- click the gear in the top right of the Rancher Desktop UI
- click Container Runtime on the right menu
- choose dockerd
- while you are there, you can check the memory/cpu in Virtual Machine, and disable kubernetes in Kubernetes (we are not using it for local development as of now, and it can be a performance bottleneck).
- click `Apply`

This automatically configures new terminals to use /var/run/docker.sock which is symbolically linked
to the rancher docker socket !!!

#### Health Check Local Docker

Open a shell and run the following to ensure docker is working
```bash
$ docker ps
CONTAINER ID   IMAGE                        COMMAND                  CREATED          STATUS          PORTS     NAMES
...

$ docker images
REPOSITORY                                                                           TAG                    IMAGE ID       CREATED         SIZE
...

$ docker run --rm hello-world

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

### Issues

Stopping Docker Desktop removes docker-compose. You will have to re-install it:
```
cd /tmp
mkdir dc-work
cd dc-work
curl -o docker-compose -kL https://github.com/docker/compose/releases/download/v2.2.3/docker-compose-darwin-x86_64
chmod +x docker-compose
sudo mv docker-compose /usr/local/bin
```

Rancher binds to 0.0.0.0, so you can route to rancher hosted images using localhost or 0.0.0.0 while using Cisco VPN. But if you want to do any other host routing, or connect to things from other computers/devices
hosted on your mac with Rancher Desktop, you will find DUHS Cisco Anyconnect clobbers local network access. You can install openconnect on a mac using brew.
```
brew install openconnect
```

Then I could connect to the vpn in a separate terminal:
```
sudo openconnect --user=$(id -un) vpn.duhs.duke.edu
password: $netidpass
password: push (or click yubikey, maybe even fingerprint)
...
```
You have to leave this running, either in the foreground, or as a background process in your terminal.

### Local Kubernetes (skip if you disabled kubernetes)
#### Mount a HostPath

You can mount a local directory on your host machine into a container running in rancher-desktop, using its full path definition (e.g. you cannot use ~/, ../, etc.). This is useful for live reloading of code that you are editing on a host application such as VSCode, or for capturing output of code generator output such as rails g.
See the [Documentation](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath-configuration-example).

An example has been provided in this repo. Rancher Desktop automatically maps permissions on files/directories created within the VM to your host user automatically, even when the container is running as root.

```bash
sed "s|PWD|$(pwd)|" test-hostpath.yaml | kubectl create -f -
kubectl attach -ti test-hostpath
ls /mounted
echo "from the container" >> /mounted/test.from.inside
exit
ls -l test.from.inside
cat test.from.inside
echo "from the host" >> test.from.inside
kubectl attach -ti test-hostpath
cat /mounted/test.from.inside
rm /mounted/test.from.inside
exit
ls -l /tmp/test-hostpath
sed "s|PWD|$(pwd)|" test-hostpath.yaml | kubectl delete -f -
```

#### Host Local Development Application on a dedicated url for your project

Rancher desktop forwards requests to 0.0.0.0 and localhost on the workstation to containers.
You do not need to create anything in /etc/hosts.

For Kubernetes Development, you will likely need to install an Ingress Controller.

Rancher Desktop comes with the [Traefik Ingress Controller](https://traefik.io/) which is enabled.
This allows you to create an [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) object in your
application (similar to an Openshift Route) configured to route all requests to the k3s DNS with a Host matching a
Ingress spec.rules[].host entry. You will also need to configure your host machine to route requests to the host to the mapped to 127.0.0.1 in the systems /etc/hosts. There are other ways to do this, but this is pretty easy. More than one domain
can be mapped to the same IP. The remove_local_development_domain.sh script removes an entry from /etc/hosts matching the domain.
Both of these scripts require sudo to run

```bash
kubectl create -f test-web-ingress.yaml
curl whoami.oasis.localhost
Hostname: whoami-658756b586-zjf65
IP: 127.0.0.1
IP: 10.42.0.17
RemoteAddr: 10.42.0.13:48154
GET / HTTP/1.1
Host: whoami.oasis.localhost
User-Agent: curl/7.64.1
Accept: */*
Accept-Encoding: gzip
X-Forwarded-For: 10.42.0.9
X-Forwarded-Host: whoami.oasis.localhost
X-Forwarded-Port: 80
X-Forwarded-Proto: http
X-Forwarded-Server: traefik-6bb96f9bd8-rfgtf
X-Real-Ip: 10.42.0.9
kubectl delete -f test-web-ingress.yaml
```
